{{Object_Title_and_Purpose
Spin driver for Sharp Memory Displays.
Model # LS013B4DN04
both 96x96 pixels reflective and 128x128 absorptive configs.
extensable to drive larger memory displays.
Supports toggling the DISP line for blanked refreshes.
Supports powering the display from an IO pin for ultra low power sleep states.
Uses CTRB to generate the EXIN signal for the display.}}


'un comment for 96x96 pixels
  lcd_p = 96              'pixels per line
  lcd_l = 96              'lines in a display }
        

'un comment for 128x128 pixels
  {lcd_p = 96              'pixels per line
  lcd_l = 96              'lines in a display }
  
'genera constants
  '_clk = 12_000_000       'my clock speed constant
  
  lcd_longs = lcd_p*lcd_l/32 'longs to alocate for buffer.  Assumes lcd_p is divisable by 32
  line_longs = lcd_p/32         '#number of longs in a line.  Assumes lcd_p is divisable by 32
  extin_freq = 2  '[Hz]
  'mem_lcd_frqx = round(4294967296.0*2.0/float(_clk))                      'frqx for 2Hz nco
  'mem_lcd_frqx = round(4294967296.0*1.0/float(_clk))                      'frqx for 1Hz nco
  msb_mask = |< 31               'just the MSB set
      

'schematic



'pin constants
  extin = 1     'set to -1 if not connected
  disp =  3     'set to -1 if not connected
  'extmd = 5     'If extin => 0 connect to Vcc, otherwise connect to gnd and call "change_vcom" at 2-60Hz with 50% duty cycle. 
  cs = 7
  mosi = 9
  sclk = 11
  vcc = 15      'set to -1 if not connected
  

  {gnd = 13      'debug used for testing only
  vin = 17      'debug used for testing only}

{Objectify the driver.
It should handle both of the 1.35 inch displays.  96x96 and 128x128
write line and write milti-line should take a pointer and display line address (and line count)
Should add a display show and hide method.
Should add a display power-up/power-down method.
Init/start should support using and IO pin for power
extmd is assumed pulled high by the circuit with extin driven by a counter unless extin pin is -1}

{with the assembly version interface via two longs, the pointer to data and the muxed line address/count
trigger the asm when line address/count is non-zero so the pointer should be written first
have both stop and start methods so that the program can fully control energy usage.}


var
  long  vcom

  
PUB init | temp, temp2
''setup the display pins and start the counter.
''also clear the display

  'power the display
  if vcc => 0
    outa[vcc] := 1              'make it high
    dira[vcc] := 1              'output
    
  'enable the display
  if disp => 0
    outa[disp] := 1             'output high
    dira[disp] := 1             'enable pin driver

  'start EXTIN frequency
  if extin => 0
    dira[extin] := 1              'setup for LCD inversion via IO pin
    frqb := posx/clkfreq*2*extin_freq'mem_lcd_frqx
    ctrb := %00100 << 26 | extin'setup ctrb as a nco

  'make communication pins outputs and low
  dira[cs] := 1
  dira[mosi] := 1
  dira[sclk] := 1
  outa[cs] := 0
  outa[mosi] := 0
  outa[sclk] := 0
  

PUB free | temp, temp2
''clear the display pins and stop the counter

  'power down the display
  if vcc => 0
    outa[vcc] := 0              'make it low
    dira[vcc] := 0              'input
    
  'disable the display
  if disp => 0
    outa[disp] := 0             'output low
    dira[disp] := 0             'disable pin driver

  'stop EXTIN frequency
  if extin => 0
    dira[extin] := 0              'stop LCD inversion via IO pin
    frqb := 0'mem_lcd_frqx
    ctrb := 0'%00100 << 26 | extin'clear ctrb 

  'make communication pins inputs and low
  dira[cs] := 0
  dira[mosi] := 0
  dira[sclk] := 0
  outa[cs] := 0
  outa[mosi] := 0
  outa[sclk] := 0


PUB disp_on
''activate the display of the per-pixel memory
  if disp => 0
    outa[disp] := 1
    

PUB disp_off
''deactivate the display of the per-pixel memory
  if disp => 0
    outa[disp] := 0


PUB power_on
''power up the display via the "vcc" IO pin
''scrambles the per-pixel memory so also calls "clear_lcd" to clear the per-pixel memory
  if vcc => 0
    outa[vcc] := 1              'restore power
    ctrb := %00100 << 26 | extin'restart ctrb as a nco
    clear_lcd
    outa[disp] := 1             're-enable pixel-memory display                 
    

PUB power_off
''shut off the display via the "vcc" IO pin
''also makes sure all IO pins to the display are output LOW with CTRB disabled.
  if vcc => 0
    outa[cs] := 0               'force serial coms outputs low
    outa[mosi] := 0
    outa[sclk] := 0    
    ctrb := 0                   'stop counter
    outa[extin] := 0            'force output pin low
    outa[disp] := 0             'disble pixel-memory display
    outa[vcc] := 0              'shut down power


pub write_screen_seg(pointer) | temp
'' writes the whole per-pixel memory from a hub buffer using several "write_milti_line" calls
'' reduces stress on the LCD due to late exin/vcom inversions
  temp := 1
  repeat lcd_l/16
    write_multi_line(pointer, temp, 16)
    pointer += line_longs*4*16
    temp += 16 

  

con
  section_break_for_serial = 1


PUB write_line(pointer, num)
'' write a line from the "pointer" location to the line "num" of the display
'' to index "pointer" through an array starting @lcd_buff use "@lcd_buf+lcd#line_longs*4*(num - 1)"
  write_multi_line(pointer,num, 1)

  

PUB write_multi_line(pointer, num, count) | temp, buf_ptr
'' write multiple lines from the "pointer" location to the display, starting at line "num" and continuing for "count" total lines.
'' for longest display life, please avoid any multi-line writes that take more than one second.
'' to index "pointer" through an array starting @lcd_buff use "@lcd_buf+lcd#line_longs*4*(num - 1)"

  num := 1 #> num <# lcd_l
  count := 1 #> count <# lcd_l
  
  temp := %1000_0000            'write a line command
  
  'set the chip select
  outa[cs] := 1
  'wait 6uS or more
  waitcnt(cnt + 1000)
  'send command
  temp := temp << 24            'msb justify command
  repeat 8
    outa[mosi] := (temp & msb_mask) or 0                  'write a bit
    outa[sclk] := 1               'pulse clock to latch bit
    outa[sclk] := 0
    temp := temp << 1           'shift to next bit


  buf_ptr := pointer' + (num-1)*(lcd_p/32)*4 'calculate starting line
    
  repeat count  
    'send address
    temp := num
    repeat 8
      outa[mosi] := (temp & 1) or 0       'write LSB
      outa[sclk] := 1             'pulse clock to latch bit
      outa[sclk] := 0
      temp := temp >> 1             'shift to next bit
     
    'send data
    repeat line_longs
      temp := long[buf_ptr]                  'read first line
      repeat 32
        'outa[mosi] := (temp & 1) or 0       'write LSB
        outa[mosi] := (temp & msb_mask) or 0
        outa[sclk] := 1             'pulse clock to latch bit
        outa[sclk] := 0
        'temp := temp >> 1             'shift to next bit
        temp := temp << 1
      buf_ptr += 4
      
    'send trailer
    outa[mosi] := 0               'clear data pin
    repeat 8
      outa[sclk] := 1             'pulse clock to finish data transfer.
      outa[sclk] := 0
     
    num += 1                    'go to next line

  'send trailer
  outa[mosi] := 0               'clear data pin
  repeat 8
    outa[sclk] := 1             'pulse clock to finish data transfer.
    outa[sclk] := 0
  
  'wait 2uS or more
  waitcnt(cnt + 1000)
  'clear the chip select
  outa[cs] := 0


PUB change_vcom   | temp
''toggle Vcom

  temp := %0000_0000__0000_0000
  vcom := not vcom
  temp := (|<14)&vcom
  
  'set the chip select
  outa[cs] := 1
  'wait 6uS or more
  waitcnt(cnt + 1000)
  'send data
  temp := temp << 16            'msb justify data
  repeat 16
    outa[mosi] := (temp & msb_mask) or 0                  'write a bit
    outa[sclk] := 1               'pulse clock to latch bit
    outa[sclk] := 0
    temp := temp << 1           'shift to next bit
  
  'wait 2uS or more
  waitcnt(cnt + 1000)
  'clear the chip select
  outa[cs] := 0



PUB clear_lcd                   | temp
''clear the LCD

  temp := %0010_0000__0000_0000
  
  'set the chip select
  outa[cs] := 1
  'wait 6uS or more
  waitcnt(cnt + 1000)
  'send data
  temp := temp << 16            'msb justify data
  repeat 16
    outa[mosi] := (temp & msb_mask) or 0                  'write a bit
    outa[sclk] := 1               'pulse clock to latch bit
    outa[sclk] := 0
    temp := temp << 1           'shift to next bit
  
  'wait 2uS or more
  waitcnt(cnt + 1000)
  'clear the chip select
  outa[cs] := 0



        