{{

┌──────────────────────────────────────────┐
│ <object Sharp Memory LCD V1.0>           │
│ Author: Forthnutter                      │               
│ See end of file for terms of use.        │                
└──────────────────────────────────────────┘

<object details, Using Propeller Demo PCB, etc.>

}}
CON

  _clkmode = xtal1 + pll16x
  _xinfreq = 5_000_000

'  kdata = 26
'  kclock = 27

'  mdata = 24
'  mclock = 25

'  vgapin = 16

'  tvpin = 12

'  laudio = 11
'  raudio = 10 

    LCD_CLK = 0
    LCD_DATA = 1
    LCD_CS = 2
    
VAR
    long lcd_clk_pin
    long lcd_data_pin
    long lcd_cs_pin
    long stack[128]

OBJ

'  k     :       "Keyboard"
'  m     :       "Mouse"
'  v     :       "VGA_Text"
'  t     :       "TV_Terminal"
'  s     :       "Synth"
   l     :       "Sharp_memory_lcd_asm_pace"
   
PUB start_up

'  k.start(kdata, kclock)
'  m.start(mdata, mclock)
'  v.start(vgapin)
'  t.start(tvpin)
    DIRA[16]~~      ' make p16 out for LED test
    OUTA[16]~
    lcd_clk_pin := LCD_CLK
    lcd_data_pin := LCD_DATA
    lcd_cs_pin := |< LCD_CS  
    l.start(@lcd_clk_pin)
    OUTA[16]~~
    repeat
        l.write_screen_seg (0)
        OUTA[16]~
    
{{
┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                   TERMS OF USE: MIT License                                                  │                                                            
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │ 
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
}}          