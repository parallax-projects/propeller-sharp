{{Object_Title_and_Purpose
Spin driver for Sharp Memory Displays.
Model # LS013B4DN04
both 96x96 pixels reflective and 128x128 absorptive configs.
extensable to drive larger memory displays.
Supports toggling the DISP line for blanked refreshes.
Supports powering the display from an IO pin for ultra low power sleep states.
Uses CTRB to generate the EXIN signal for the display.}}


'un comment for 96x96 pixels
  lcd_p = 96              'pixels per line
  lcd_l = 96              'lines in a display }
        

'un comment for 128x128 pixels
  {lcd_p = 96              'pixels per line
  lcd_l = 96              'lines in a display }
  
'general constants
  lcd_longs = lcd_p*lcd_l/32 'longs to alocate for buffer.  Assumes lcd_p is divisable by 32
  line_longs = lcd_p/32         '#number of longs in a line.  Assumes lcd_p is divisable by 32
  extin_freq = 2  '[Hz]
  msb_mask = |< 31               'just the MSB set

'speed constants
  clk_width = 20  '[clocks] width of clock pulses
  delay_width = 10              '[clocks] delay to insert between pin changes.  must be greater than 6.  to calculate half baud add ~18 clocks.      

'schematic



'pin constants
'  extin = 1     'set to -1 if not connected
'  disp =  3     'set to -1 if not connected
  'extmd = 5     'If extin => 0 connect to Vcc, otherwise connect to gnd and call "change_vcom" at 2-60Hz with 50% duty cycle. 
'  cs = 7
'  mosi = 9
'  sclk = 11
'  vcc = 15      'set to -1 if not connected
  

  {gnd = 13      'debug used for testing only
  vin = 17      'debug used for testing only}

{Objectify the driver.
It should handle both of the 1.35 inch displays.  96x96 and 128x128
write line and write milti-line should take a pointer and display line address (and line count)
Should add a display show and hide method.
Should add a display power-up/power-down method.
Init/start should support using and IO pin for power
extmd is assumed pulled high by the circuit with extin driven by a counter unless extin pin is -1}

{with the assembly version interface via two longs, the pointer to data and the muxed line address/count
trigger the asm when line address/count is non-zero so the pointer should be written first
have both stop and start methods so that the program can fully control energy usage.}

{{ refactor this code to run at RCFAST and crystal speeds from 5-80MHz
This will mainly require adding delays.
Optionally stop using the counters for SPI anymore. }}


var
  long  vcom
  long  cog
  long  data_ptr      'Hub pointer to new data
  long  addr_cnt      'muxed line count and display line address.  [16|16] [count|line address] Initiates a transfer if greater than zero.
                      'addr_cnt = -1 toggles the VCOM bit.
                      'addr_cnt = -2 clears the LCD memory.

  
PUB start(pointer) | temp, temp2
''setup the display pins and start the counter.
''also clear the display

  'power the display
'  if vcc => 0
'    outa[vcc] := 1              'make it high
'    dira[vcc] := 1              'output
    
  'enable the display
'  if disp => 0
'    outa[disp] := 1             'output high
'    dira[disp] := 1             'enable pin driver

  'start EXTIN frequency
'  if extin => 0
'    dira[extin] := 1              'setup for LCD inversion via IO pin
'    frqb := posx/clkfreq*2*extin_freq'mem_lcd_frqx
'    ctrb := %00100 << 26 | extin'setup ctrb as a nco

  'configure some constants
  'six_us := clkfreq*6/1_000_000

  cog := cognew(@entry, pointer)          'start the new cog now, assembly cog at "entry" label.
  'make communication pins outputs and low
  'move to ASM initilization
  {dira[cs] := 1
  dira[mosi] := 1
  dira[sclk] := 1
  outa[cs] := 0
  outa[mosi] := 0
  outa[sclk] := 0}
  

PUB stop | temp, temp2
''clear the display pins and stop the counter

  'power down the display
'  if vcc => 0
'    outa[vcc] := 0              'make it low
'    dira[vcc] := 0              'input
    
  'disable the display
'  if disp => 0
'    outa[disp] := 0             'output low
'    dira[disp] := 0             'disable pin driver

  'stop EXTIN frequency
'  if extin => 0
'    dira[extin] := 0              'stop LCD inversion via IO pin
'    frqb := 0'mem_lcd_frqx
'    ctrb := 0'%00100 << 26 | extin'clear ctrb 

  if cog
    cogstop(cog~ - 1)                                   'if the driver is already running, stop the cog

  
  'make communication pins inputs and low
  'move to ASM initilization
  {dira[cs] := 0
  dira[mosi] := 0
  dira[sclk] := 0
  outa[cs] := 0
  outa[mosi] := 0
  outa[sclk] := 0    '}


'PUB disp_on
''activate the display of the per-pixel memory
'  if disp => 0
'    outa[disp] := 1
    

'PUB disp_off
''deactivate the display of the per-pixel memory
'  if disp => 0
'    outa[disp] := 0


{PUB power_on   'use "start" and "stop" instead.
''power up the display via the "vcc" IO pin
''scrambles the per-pixel memory so also calls "clear_lcd" to clear the per-pixel memory
  if vcc => 0
    outa[vcc] := 1              'restore power
    ctrb := %00100 << 26 | extin'restart ctrb as a nco
    clear_lcd
    outa[disp] := 1             're-enable pixel-memory display                 
    

PUB power_off
''shut off the display via the "vcc" IO pin
''also makes sure all IO pins to the display are output LOW with CTRB disabled.
  if vcc => 0
    outa[cs] := 0               'force serial coms outputs low
    outa[mosi] := 0
    outa[sclk] := 0    
    ctrb := 0                   'stop counter
    outa[extin] := 0            'force output pin low
    outa[disp] := 0             'disble pixel-memory display
    outa[vcc] := 0              'shut down power       }

pub write_screen_seg(pointer)
'' writes the whole per-pixel memory from a hub buffer using several "write_milti_line" calls
'' reduces stress on the LCD due to late exin/vcom inversions
  data_ptr := pointer
  addr_cnt := wr_seg_cmd
  repeat while addr_cnt         'wait for completion


{pub write_screen_seg_old(pointer) | temp
'' writes the whole per-pixel memory from a hub buffer using several "write_milti_line" calls
'' reduces stress on the LCD due to late exin/vcom inversions
  temp := 1
  repeat constant(lcd_l/16)
    write_multi_line(pointer, temp, 16)
    pointer += constant(line_longs*4*16)
    temp += 16                     '}


PUB write_line(pointer, num)
'' write a line from the "pointer" location to the line "num" of the display
'' to index "pointer" through an array starting @lcd_buff use "@lcd_buf+lcd#line_longs*4*(num - 1)"
  write_multi_line(pointer,num, 1)


pub write_multi_line(pointer,num,count)
'' write multiple lines from the "pointer" location to the display, starting at line "num" and continuing for "count" total lines.
'' for longest display life, please avoid any multi-line writes that take more than one second.
'' to index "pointer" through an array starting @lcd_buff use "@lcd_buf+lcd#line_longs*4*(num - 1)"
 'comment out bounds checks once code is tested.
  {num := 1 #> num <# lcd_l
  count := 1 #> count <# lcd_l '}
  data_ptr := pointer           'pass the address
  addr_cnt := ( count << 16 ) | num                     'pass the command.  Starts a serial transfer.
  repeat while addr_cnt         'wait for completion
  

pub change_vcom
''toggle Vcom 
  addr_cnt := Vcom_cmd          'send the command to the ASM
  repeat while addr_cnt         'wait for completion
  
PUB clear_lcd                  | temp
''clear the LCD
  addr_cnt := clr_lcd_cmd       'send the command to the ASM
  repeat while addr_cnt         'wait for completion
  


con
  section_break_for_serial = 1


dat
entry         'initalize the pins and counters
              mov       p1,par
              rdlong    sclk,p1
              mov       sclk_mask,#1
              rol       sclk_mask,sclk
              add       p1,#4
              rdlong    mosi,p1
              mov       mosi_mask,#1
              rol       mosi_mask,mosi
              add       p1,#4
              rdlong    cs_mask,p1
              add       p1,#4
              rdlong    address,p1
              
              or        dira,   cs_mask         'set serial pins to outputs
              or        dira,   mosi_mask
              or        dira,   sclk_mask
              movi      ctra_mask,#%00100000
              movs      ctra_mask,sclk
              
              movi      ctrb_mask,#%00100000
              movs      ctrb_mask,mosi
              
              'CTRA setup for clock pulses
              mov       frqa,   #1
              mov       phsa,   #0
              mov       ctra,   ctra_mask              
              'CTRB setup to shift out data.  Places bit31 of phsb on the output pin.
              mov       frqb,   #0
              mov       phsb,   #0
              mov       ctrb,   ctrb_mask

cmd_return    mov       addr_cnt, #0            'clear commands and pointer
              mov       data_ptr, #0
              mov       t1, address
              'add       t1, #4
              wrlong    data_ptr, t1            'clear command and pointer in hub as well.             
              wrlong    addr_cnt,              
command       'wait for addr_cnt <> 0
              rdlong    addr_cnt,     par             'read in command
              mov       phsa,   #0              'keep CTRA from rolling over accidentally
              tjz       addr_cnt,     #command        'if command is zero, read it again.
              'branch to the correct code.
              cmps      addr_cnt,     Vcom_cmd        wz
              rdlong    data_ptr, t1            'read in the source data HUB pointer.  (located here to for HUB sync)
        if_z  call       #Vcom_asm                       'Vcom code
              cmps      addr_cnt,     clr_lcd_cmd     wz
        if_z  call       #clr_lcd_asm                    'clr_lcd code
              cmps      addr_cnt,     wr_seg_cmd        wz
        if_z  call      #wr_seg_asm             'write a whole screen in segments                       
              cmps      addr_cnt,     #0        wc
        if_nc call      #wr_mul_line            'code to send multiple-lines of data
              jmp       #cmd_return             'restart the command wait.


''writes data out to the display
''ToDo: unroll the inner loops for speed.  Should drop the screen refresh time from ~7sec to ~4.5sec.
''ToDo eventually: try to get the SPI working with a free-running clock counter.  Potential for 2-3 sec screen refreshes.
wr_mul_line   mov       line_count,     addr_cnt              'copy the command
              shr       line_count,     #16             'shift upper 16-bits into lower 16-bits. line count
              mov       lcd_line,       addr_cnt               
              and       lcd_line,     low_word_mask   'mask off upper 16-bits. LCD line number
              'Serial data transfer code.
              or        outa,   cs_mask         'select the display SPI
              'nop       'wait 6us
              mov       t1,     #480-10
              call      #pause_clk
              
              mov       idx,    #8              'setup for 8 bits
              mov       phsb, write_line_lcmd   'write data for first command bit

              mov       t1,     #delay_width
              call      #pause_clk
              
:loop         absneg    phsa,   #clk_width              '4 clock cycle clk pulse
              mov       t1,     #delay_width
              call      #pause_clk
              rol       phsb,   #1              'move to next bit
              mov       t1,     #delay_width
              call      #pause_clk
              djnz      idx,    #:loop          'loop.
              'write out line address
line_loop     mov       phsb,   lcd_line        'move address into output counter
              mov       idx,    #8              'setup for 8 bits
:loop         ror       phsb,   #1              'shift first bit to position
              mov       t1,     #delay_width
              call      #pause_clk
              absneg    phsa,   #clk_width              'pulse the clock
              mov       t1,     #delay_width
              call      #pause_clk
              djnz      idx,    #:loop          'loop
              'writ out line data
              mov       idx2,   #line_longs     'setup to transfer one whole line
line_data     mov       idx,    #32             'setup for 32-bits
              rdlong    phsb,   data_ptr        'read the first 32 bits from the HUB
              mov       t1,     #delay_width
              call      #pause_clk
:loop         absneg    phsa,   #clk_width              'pulse the clock    
              mov       t1,     #delay_width
              call      #pause_clk
              rol       phsb,   #1              'shift to next bit
              mov       t1,     #delay_width
              call      #pause_clk
              djnz      idx,    #:loop          'loop
              add       data_ptr, #4            'increment the HUB pointer
              djnz      idx2,   #line_data      'loop for 3-4 longs of data
              'send trailer
              mov       phsb,   #0              'clear the data pin
              mov       idx,    #8              'prep for 8 extra clocks
              mov       t1,     #delay_width
              call      #pause_clk
:loopt        absneg    phsa,   #clk_width              'pulse clock
              mov       t1,     #80
              call      #pause_clk
              djnz      idx,    #:loopt         'loop
              add       lcd_line, #1            'go to next line
              djnz      line_count, #line_loop  'do more lines if needed
              'totaly done now, send another trailer
              mov       phsb,   #0              'clear the data pin
              mov       idx,    #8              'prep for 8 extra clocks
:loopt2       absneg    phsa,   #clk_width              'pulse clock
              mov       t1,     #80
              call      #pause_clk
              djnz      idx,    #:loopt2         'loop
              'de-select the display
              andn      outa,   cs_mask         'de-select the display 
wr_mul_line_ret ret          'done with transfer go back to waiting for a command.
        
              
'' writes the whole per-pixel memory from a hub buffer using several "write_milti_line" calls
'' reduces stress on the LCD due to late exin/vcom inversions
seg_idx       long      0
seg_temp      long      0
wr_seg_asm    mov       seg_temp, #1
              mov       seg_idx, #(lcd_l/16)
:loop         mov       addr_cnt, #16           '16 rows at a time
              shl       addr_cnt, #16           'move to top word
              or        addr_cnt, seg_temp      'mux in starting display line
              call      #wr_mul_line            'squirt out the data
              'add       data_ptr, #(line_longs*4*16)'advance hub data pointer 16 lines. (already done in data loop)
              add       seg_temp, #16           'advance 16 lines 
              djnz      seg_idx, #:loop         'loop
              mov       addr_cnt, wr_seg_cmd            'restore command state              
wr_seg_asm_ret ret

'' toggle the Vcom bit inside the display
Vcom_state    long      0
Vcom_asm      'toggle the Vcom bit
              or        outa,   cs_mask
              mov       t1,     #480-10
              call      #pause_clk
              mov       idx,    #16
              xor       Vcom_state, Vcom_lcmd   'toggle state.
              mov       phsb,   Vcom_state
              mov       t1,     #delay_width
              call      #pause_clk
:loop         absneg    phsa,   #clk_width
              mov       t1,     #delay_width
              call      #pause_clk
              rol       phsb,   #1
              mov       t1,     #delay_width
              call      #pause_clk
              djnz      idx,    #:loop
              andn      outa,   cs_mask
Vcom_asm_ret  ret     'done with transfer go back to waiting for a command.

'' send the clear LCD command to the display.
clr_lcd_asm   'send the clear LCD command
              or        outa,   cs_mask         'select the display SPI
              mov       t1,     #480-10
              call      #pause_clk
              mov       idx,    #16             'prep for 16-bits
              mov       phsb,   clr_lcd_lcmd    'set first bit of command
              mov       t1,     #delay_width
              call      #pause_clk
:loop         absneg    phsa,   #clk_width              'pulse the clock pin
              mov       t1,     #delay_width
              call      #pause_clk
              rol       phsb,   #1              'set next bit
              mov       t1,     #delay_width
              call      #pause_clk
              djnz      idx,    #:loop          'loop
              andn      outa,   cs_mask         'deselect the display
clr_lcd_asm_ret         ret  'done with transfer go back to waiting for a command.


'' wait for t1 + 10 clocks to elapse.  
pause_clk
              add       t1, cnt
              waitcnt   t1, #0
pause_clk_ret ret


'communication variables
'PAR points to the hub location of the first variable here
addr_cnt      long      0-0     'muxed line count and display line address.  [16|16] [count|line address] Initiates a transfer if greater than zero.
                                'addr_cnt = -1 toggles the VCOM bit.
                                'addr_cnt = -2 clears the LCD memory.
data_ptr      long      0-0     'Hub pointer to new data



'initialised variables
'cs_mask       long      |<cs
'mosi_mask     long      |<mosi
'sclk_mask     long      |<sclk
ctra_mask     long      %00100 << 26 | sclk
ctrb_mask     long      %00100 << 26 | mosi
Vcom_cmd      long      -1
clr_lcd_cmd   long      -2
wr_seg_cmd    long      -3
low_word_mask long      $0000_FFFF
'six_us        long      0-0
write_line_lcmd long    %1000_0000 << 24        'MSB justified for easy serial output
clr_lcd_lcmd  long      %0010_0000__0000_0000 << 16 'MSB justified for easy serial output
Vcom_lcmd     long      (|<14) << 16            'MSB justified for easy serial output      


'volitile variables
cs_mask     res 1
mosi        res 1
mosi_mask   res 1
sclk        res 1
sclk_mask   res 1
lCD_line    res 1
line_count  res 1
address     res 1
p1          res 1
t1      res   1
t2      res   1
t3      res   1
t4      res   1
idx     res   1
idx2    res   1


{PUB change_vcom_old   | temp
''toggle Vcom 

  temp := %0000_0000__0000_0000
  vcom := not vcom
  temp := (|<14)&vcom
  
  'set the chip select
  outa[cs] := 1
  'wait 6uS or more
  waitcnt(cnt + 1000)
  'send data
  temp := temp << 16            'msb justify data
  repeat 16
    outa[mosi] := (temp & msb_mask) or 0                  'write a bit
    outa[sclk] := 1               'pulse clock to latch bit
    outa[sclk] := 0
    temp := temp << 1           'shift to next bit
  
  'wait 2uS or more
  waitcnt(cnt + 1000)
  'clear the chip select
  outa[cs] := 0



PUB clear_lcd_old                   | temp
''clear the LCD

  temp := %0010_0000__0000_0000
  
  'set the chip select
  outa[cs] := 1
  'wait 6uS or more
  waitcnt(cnt + 1000)
  'send data
  temp := temp << 16            'msb justify data
  repeat 16
    outa[mosi] := (temp & msb_mask) or 0                  'write a bit
    outa[sclk] := 1               'pulse clock to latch bit
    outa[sclk] := 0
    temp := temp << 1           'shift to next bit
  
  'wait 2uS or more
  waitcnt(cnt + 1000)
  'clear the chip select
  outa[cs] := 0      '}


{PUB write_multi_line_old(pointer, num, count) | temp, buf_ptr
'' write multiple lines from the "pointer" location to the display, starting at line "num" and continuing for "count" total lines.
'' for longest display life, please avoid any multi-line writes that take more than one second.
'' to index "pointer" through an array starting @lcd_buff use "@lcd_buf+lcd#line_longs*4*(num - 1)"

  num := 1 #> num <# lcd_l
  count := 1 #> count <# lcd_l
  
  temp := %1000_0000            'write a line command
  
  'set the chip select
  outa[cs] := 1
  'wait 6uS or more
  waitcnt(cnt + 1000)
  'send command
  temp := temp << 24            'msb justify command
  repeat 8
    outa[mosi] := (temp & msb_mask) or 0                  'write a bit
    outa[sclk] := 1               'pulse clock to latch bit
    outa[sclk] := 0
    temp := temp << 1           'shift to next bit


  buf_ptr := pointer' + (num-1)*(lcd_p/32)*4 'calculate starting line
    
  repeat count  
    'send address
    temp := num
    repeat 8
      outa[mosi] := (temp & 1) or 0       'write LSB
      outa[sclk] := 1             'pulse clock to latch bit
      outa[sclk] := 0
      temp := temp >> 1             'shift to next bit
     
    'send data
    repeat line_longs
      temp := long[buf_ptr]                  'read first line
      repeat 32
        'outa[mosi] := (temp & 1) or 0       'write LSB
        outa[mosi] := (temp & msb_mask) or 0
        outa[sclk] := 1             'pulse clock to latch bit
        outa[sclk] := 0
        'temp := temp >> 1             'shift to next bit
        temp := temp << 1
      buf_ptr += 4
      
    'send trailer
    outa[mosi] := 0               'clear data pin
    repeat 8
      outa[sclk] := 1             'pulse clock to finish data transfer.
      outa[sclk] := 0
     
    num += 1                    'go to next line

  'send trailer
  outa[mosi] := 0               'clear data pin
  repeat 8
    outa[sclk] := 1             'pulse clock to finish data transfer.
    outa[sclk] := 0
  
  'wait 2uS or more
  waitcnt(cnt + 1000)
  'clear the chip select
  outa[cs] := 0 '}




        