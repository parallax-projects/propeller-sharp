{{Spin driver for Sharp memory display, mLCD,  http://www.sharpmemorylcd.com/
specifically LS013B7DH05, 144x168 pixel resolution in a active area 
Adafruit 3502 PCB interface.

References:
  http://www.sharpmemorylcd.com/resources.html
  
The mLCD and this object:
3 volt power, ~ 50 microwatt, 17 microamp to hold steady display
Pin connections to mLCD:
  Vdd, Vdda  to +3V dc
  Vss, Vssa to ground
  SCS (active high), SI, SCLK     for SPI, one-way only master to display
  EXTCOMM     for connection to a Prop ctrb output that will toggle at a low rate to commute the backplane polarity
  EXTMODE pin is tied to 3Vdd, so the polarity update will come from the EXTCOMM pin
  DISP    to blank the display during updates, not used in this object, tie the pin high to enable the display
It would be possible to power the screen from a Prop pin.
Also possible to control the Disp pin.  Not implemented here.


<CMD><ADDRESS><DATA>
CMD 8BITS
ADDRESS 8BITS
DATA 144 bits = 18 Bytes

}}


CON
	XSIZE = 144
	YSIZE = 168
    PIXELS_PER_LINE = 144             'pixels per line
    LINES_PER_LCD = 168              'lines in a display
    NUMBER_OF_LINES = 168
    DIV_VALUE = 8
    BYTES_PIXELS_SIZE = PIXELS_PER_LINE/DIV_VALUE   '144 divides better with 8 bits
    LINE_SIZE = BYTES_PIXELS_SIZE + 2               'number of bytes to send                                          '
    EXTCOMM_FREQ = 5                            '[Hz] for polarity reversals

	LCD_MEMORY_SIZE = LINE_SIZE * NUMBER_OF_LINES
	
    LCD_MAINTAIN = %00000000
    LCD_REFRESH  = %00000001
    LCD_CLEAR    = %00000100
    

  SMALL = 0              ' pixels high, 8 wide, 16 chars on one line
  LARGE = 1               ' pixels high, 16 wide, 8 chars on one line

  NORMAL = -1
  INVERSE = 0

VAR

'  byte scs, mosi, sclk, extcomm
	byte lcd_memory[LCD_MEMORY_SIZE]
    word lcd_line_pointer[NUMBER_OF_LINES]
	byte pixelLineBuf[LINE_SIZE]
    long scs_pin
    long sclk_pin
    long ssi_pin
    long command
	long lcd_pointer

OBJ
  serial : "FullDuplexSerial"

  
  
PUB Init(_sclk, _mosi, _scs, _extcomm) | idy, idx
''setup the display pins and start the counter.
''also clears the display
  scs_pin := _scs                   ' chip select,active high
  ssi_pin := _mosi                 ' spi out from Prop, input to mlcd
  sclk_pin:= _sclk                  ' spi clock, pulses high-low to latch data
'  extcomm := _extcomm           ' Prop counter NCO output, polarity reversals for screen

	repeat idy from 0 to NUMBER_OF_LINES
		idx := LINE_SIZE * idy
		lcd_line_pointer[idy] := @lcd_memory[idx]
		
		
  'start extcomm frequency
'  dira[extcomm] := 1                                    'setup for LCD inversion via IO pin
'  frqb := posx/clkfreq*2*EXTCOMM_FREQ                   'mem_lcd_frqx
'  ctrb := %00100 << 26 | extcomm                        'setup ctrb as a nco

    serial.start_default(115200)
    serial.printf(string("*** mLCD Demo ***\n"))
    cognew(@entry,@scs_pin)                    'if the driver is already running, stop the cog

  'make communication pins outputs and low
  'dira[scs] := 1
  'dira[mosi] := 1
  'dira[sclk] := 1
  'outa[scs] := 0
  'outa[mosi] := 0
  'outa[sclk] := 0
  'Clear

' get the normal value
PUB Norm
	return NORMAL

' get the inverse value	
PUB Invert
	return INVERSE

PUB Smallish
	return SMALL
	
PUB Largeish
	return LARGE

PUB Free
''clear the display pins and stop the counter
  Clear
'  dira[extcomm] := 0
  frqb := 0                               '
  ctrb := 0
  'make communication pins input, low
'  dira[scs] := 0
'  dira[mosi] := 0
'  dira[sclk] := 0
'  outa[scs] := 0
'  outa[mosi] := 0
'  outa[sclk] := 0

PUB DrawLine(pointer,lcdLine) | buf_ptr
    repeat until (command == 0)
    	'serial.printf(string("cmd not zero %x\n"),command)
    
    'lcdline := lcdline #> 0 <#168
    pixelLineBuf[0] := LCD_REFRESH              ' lsb first, 1 is the 1st bit out
    pixelLineBuf[1] := lcdLine+1
	lcd_pointer := @pixelLineBuf
    command := $01000016   '
'	serial.printf(string("cmd sent %x\n"),command)    
    'repeat while (command <> 0)

PUB Clear | temp
''clear the LCDto all white
    repeat until (command == 0)
		'serial.printf(string("cmd not zero %x\n"),command)
		
    pixelLineBuf[0] := LCD_CLEAR
    temp := $01000002    'two bytes
    command := temp



PRI swap(a, b) | t
	
	t := a
	a := b
    b := t



'***********************************************************************
PUB DrawPixel(screen,x,y,color)
	if x < 0 OR x >= XSIZE OR y < 0 OR y >= YSIZE
    	return

  	case rotation
  		1:
    		swap(x, y)
    		x := WIDTH - 1 - x
    	2:
		    x := WIDTH - 1 - x
		    y := HEIGHT - 1 - y
		3:
			swap(x, y)
			y := HEIGHT - 1 - y
	
	if color
  		sharpmem_buffer[(y * WIDTH + x) / 8] |= pgm_read_byte(&set[x & 7]);
	else
		sharpmem_buffer[(y * WIDTH + x) / 8] &= pgm_read_byte(&clr[x & 7]);

   

PUB DrawText(lcdLine, strptr, mode , fontsize) : buffer | pixels, fontLine, idx, code, loc, stepSize, lastChar, pixelsPerWidth, adrs, pixel, nlines, nullflag
' write the null terminated text passed in at pointer strptr to the LCD at the given location
' lcdLine is a value from 0 to 127 that divides the 128 screen lines into 8 segments as the upper left corner of the text.
' given LARGE and SMALL text options, the normal starting points will be values 0,16,32,48,64,80,96,112
' mode is either NORMAL for black on white or INVERSE for white on black
' fontsize is either LARGE=1 (32x16 pixels 8 chars per line), SMALL=0 (16x8 pixels, 16 chars per line)
    lcdline := lcdline #> 0 <#168
    stepSize := 2-fontsize                            ' stepSize is either 2 for small font or 1 for large font
    lastChar := stepsize * 8 - 1                      ' last char index is 15 for small font or 7 for large font
    pixelsPerWidth := (fontsize+1) * 8                ' character width is either 8 for small font or 16 for large font
    nlines:= pixelsPerWidth * 2 - 1                    ' 16 or 32 lines for the font
    serial.printf(string("nlines %d\n"),nlines)
    repeat fontLine from 0 to nlines                  ' step through the lines of the font
        nullflag := false                                ' helps when input zString is shorter than the field
        repeat idx from 0 to lastChar                    ' step through 8 or 16 characters in the line
            loc := @pixelLineBuf[2] + (idx<<(fontSize))       'this is the location in the pixel buffer for the pixels of each char on the line, byte or word address
            code := byte[strptr+idx]                       ' this is the ascii character to evaluate.
            if (code == 0) or nullflag    
                code := 32                                   ' if zString is less than 8 or 16 chars, do spaces
                nullflag := true
            adrs := (code)/2 * 128 + 32768                 ' address of the ascii code in the ROM table.
            pixels := long[adrs][fontLine<<(stepsize-1)]   '
            pixels >>= (code // 2) ' ascii codes are interleaved, odd/even pixels, shift right one bit for an odd ascii code
            repeat pixelsPerWidth
                pixel := pixels & 1
                pixels >>=  (stepSize << 1)                  ' every other bit for large font, every 4th for small font
                buffer := (buffer<<1) + pixel
            buffer ><= pixelsPerWidth                      'to flip the bits order
            if mode                                        ' if inverse white on black
                !buffer
            if fontSize                                    ' large font
                word[loc] := buffer                          ' writes the result into pixelLineBuffer
            else
                byte[loc] := buffer                          ' small font
     	DrawLine(@pixelLineBuf, lcdLine)           'writes the 128 pixel line to the LCD screen, repeat for number of horizontal lines
        lcdLine := lcdLine + 1

        
    

PUB DrawBar(lcdline, length, mode, thickness) | idx, integerpart, fractionpart
' lcdLine is a value from 0 to 127
' length is a value from 0 to 127 for the length of the horizontal bar
' thickness is the number of lines to be included in the bar.
' mode is either NORMAL= -1  for black on white or INVERSE= 0 for white on black.
  lcdline := (lcdline #> 0 <#167)
  length := length #> 0 <# 143
  integerpart := length/32
  fractionpart := length//32
  longfill(@pixelLineBuf[2], mode, 4)
  if integerpart
    longfill(@pixelLineBuf[2], !mode, integerpart)
  long[@pixelLineBuf][integerPart] := ((|< fractionpart) - 1) ^ mode
  repeat idx from 1 to thickness   ' while lcdline is 0 to 127, display wants 1 to 128
    DrawLine(@pixelLineBuf, lcdLine+idx)

PUB DrawDot(lcdline, x, mode, thickness)  | idx, x1, x2, data[18]
' lcdLine is a value from 0 to 127 (will be 1 to 128 when sent to mlcd)
' x is the horizontal coordinate of the "dot"
' thickness is the length of the "dot".  This method prints a line one pixel thick vertically.
' mode is either NORMAL= -1  for black on white or INVERSE= 0 for white on black.
    longfill(@data,0,6)   ' this is a local 4-word line buffer, all $ff for white background
    x1 := x/32   ' 0,1,2, or 3  index of long array        ......./......./......./.......x
    x2 := x//32  ' remainder from 0 to 31
    result := ((|< (thickness #> 1)) - 1) << x2
    data[x1] := result
    if (idx := x2+thickness - 32 ) > 0
      data[x1+1] := |< idx - 1
    ' at this point the line is white on black
    if mode == NORMAL
      repeat idx from 0 to 3
        !data[idx] ' change to white on black if mode = NORMAL
    DrawLine(@data, lcdLine)

PRI Spi(value, Nbits)
'    repeat Nbits
'      outa[mosi] := value      'write lsbit
'      outa[sclk] := 1             'pulse clock to latch bit
'      outa[sclk] := 0
      value := value >> 1             'shift to next bit


' This driver works by grab data from Hub Memory and spitting it out the spi out routine
' command is hub memory that holds the function of data lower 8 bits is number bytes of data
'
DAT
  'spaces byte "               " ' in case a null terminated text string is less than the field width of 8 or 16
            org
entry
            mov p1,par          ' save par in p1 to get the structure
            rdlong cs_pin,p1    ' get cs pin from structure
            mov cs_mask,#1      ' set up mask
            shl cs_mask,cs_pin  ' the cs_mask pin is set
            add p1,#4           ' increment structure pointer
            rdlong clk_pin,p1   ' get clk pin from structure
            mov clk_mask,#1     ' set up mask
            shl clk_mask,clk_pin    ' clk_mask is now set
            add p1,#4               ' increment structure pointer
            rdlong si_pin,p1        ' get the serial in pin
            mov si_mask,#1          ' set mask to start with
            shl si_mask,si_pin      ' serial in pin mask ready
            add p1,#4               ' pointer incremented
            mov cmd_address,p1      ' get the command status
            add p1,#4               ' increment address
            mov display_address,p1	' save the address
            
            andn outa,cs_mask       ' make sure cs pin is set low
            or dira, cs_mask        ' set pin direction to output
            andn outa,clk_mask      ' make sure clk is low
            or dira,clk_mask        ' clock is now output
            andn outa,si_mask       ' si pin is low
            or dira, si_mask        ' si pin now output
            andn outa,led_status    ' this is general diagnostic suff
            or dira,led_status      ' make sure it on the our
            'mov p1,#$ff
            'call #led_set
            wrlong lclear,cmd_address

loop
            'jmp #loop
            rdlong p1,cmd_address   ' get the command
            tjz p1,#loop
            mov p2,p1               ' move things
            and p2,#$ff             ' we only want lower 8 bits
            tjz p2,#loop
            shr p1,#24              ' move the upper 8 bits
            cmp p1,#1   wz          ' update with data
            rdlong p1,display_address
    if_z    call #spi_bytes

            wrlong lclear,cmd_address ' clear cmd when finised
            rdlong p1,cmd_address
            call #led_set
            jmp #loop

' Send out the data SPI way
' p1 is the data to send max 32 bits
' p2 is the number of bit to send
' Make sure CS is high before calling
spi_write
            rcr p1,#1   wc
            muxc outa,si_mask       ' what ever the C is out via mask
            mov d1,ms
            call #delay
            or outa,clk_mask        ' clk high
            mov d1,ms
            call #delay
            andn outa,clk_mask      ' clk low
            mov d1,ms
            call #delay
            djnz p2,#spi_write       ' loop
spi_write_ret
            ret


' send out the all the display array
spi_array
			mov lines,#NUMBER_OF_LINES	'lcd_lines
spialoop
			mov p1,display_address
			mov p2,#LINE_SIZE
			call #spi_bytes
			add display_address,#4
			djnz lines,#spialoop			
spi_array_ret
			ret

' send 144 bits to SPI
' p1 address of byes to display
' p2 number of bytes
spi_bytes
            '             ' turn on led for test
            or outa,cs_mask
            mov d1,cs_delay
            call #delay
            mov px,p2                  ' save number of bytes need p2
            mov pa,p1                   ' save p1 we need it
spi1
            rdbyte p1,pa                ' get value from display address
            'call #led_set
            mov p2,#8
            call #spi_write             ' send out 32
            add pa,#1
            djnz px,#spi1
            mov d1,cs_delay
            call #delay
            andn outa,cs_mask
spi_bytes_ret
            ret
            
' set status LEDS
' p1 has line number
led_set
            mov p3,p1                   ' save p1
            mov p4,outa
            andn p4,led_status
            and p3,#$ff
            shl p3,#16
            or p4,p3
            mov outa,p4
            mov d1,delay_value
            'call #delay
led_set_ret
            ret
            
            
delay
            add d1, cnt
            waitcnt d1, delay_value
delay_ret
            ret
 
'                    33222222 22221111 11111100 00000000 
'                    10987654 32109876 54321098 76543210
led_p16     long    %00000000_00000001_00000000_00000000 
led_status  long    %00000000_11111111_00000000_00000000

delay_value long    70000000
ms          long    10
cs_delay    long    1000
lclear		long	0
lcd_lines   long	168
            
p1          res 1                   ' general parameter 1
p2          res 1                   ' general parameter 2
p3          res 1
p4          res 1
px          res 1                   ' general index
pa          res 1                   ' general address
d1          res 1
d2          res 1
lines		res 1
cs_pin      res 1                   ' chip select pin number
cs_mask     res 1                   ' chip select mask
clk_pin     res 1                   ' clock pin
clk_mask    res 1                   ' clock mask pin
si_pin      res 1                   ' serial in pin
si_mask     res 1                   ' serial in mask pin
cmd_address res 1                   ' current command status address
cmd_status  res 1                   ' current command status
display_address res 1               ' display address

            FIT
