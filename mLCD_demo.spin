
{{Demo of the Spin driver for the Sharp memory display, mLCD,
copyright (c) 2016 EME Systems. See end of file for terms of use.
Author, Tracy Allen, Manpreet Gill, EME Systems
Sharp memory displays: http://www.sharpmemorylcd.com/
specifically LS013B7DH03,  128x128 pixel resolution in a active area 23.04mm square
See forums thread:
  http://forums.parallax.com/discussion/159289/looking-for-a-recommendation-on-a-low-power-display-updated-with-results
  Code by Marty Lawsen and John Rucker from the above thread informed this object
The mLCD connections:
3 volt power, ~ 50 microwatt, 17 microamp to hold steady display
Pin connections to mLCD:
  Vdd, Vdda  to +3V dc
  Vss, Vssa to ground
  SCS (active high), SI, SCLK     for SPI, one-way only master to display
  EXTCOMM     for connection to a Prop ctrb output that will toggle at a low rate to commute the backplane polarity
  EXTMODE pin is tied to 3Vdd, so the polarity update will come from the EXTCOMM pin
  DISP    to blank the display during updates, not used in this object, tie the pin high to enable the display
See the object header and methods for detailed info.

This Demo after initialization executes a lineup of text and graphic methods.

}}

CON

' Set pins and clock mode as appropriate.

{{  ksums remember 10Mhz pll8
  _clkmode = xtal2 + pll8x                '
  _xinfreq = 10_000_000
  SCLK = 0
  SI  = 1
  CS = 2
  EXTIN = 3
}}

{{  pats   5MHz pll16 }}
  _clkmode = xtal1 + pll16x                '
  _xinfreq = 5_000_000
  SCLK = 0
  SI  = 1
  CS = 2
  EXTIN = 3


VAR
  byte recordbuffer[50]

OBJ
  mLCD : "mlcd_spin_driver"

PUB Demo

    mLCD.Init(SCLK, SI, CS, EXTIN) ' lcd initialization & clear

    repeat
        SplashScreen
  		Numerics
  		Randoms
  		Sinusoids
  		WaveFonts
  		LargeFontList
		SmallFontList
		'SquaresTriangles
		'ScreenSaver

PUB SplashScreen
  mlcd.clear
  mlcd.drawtext(0,string("*** mLCD Demo ***"),mlcd.Norm,mlcd.Smallish)
  mlcd.drawtext(16,string("       by       "),mlcd#NORMAL,mlcd#SMALL)
  DrawEME(mlcd#NORMAL)
  Pause(1500)

PUB Numerics
' ----- numeric value patterns, random number values using object recordbuilder
  mLCD.Clear
  mlcd.drawtext(0,string("Values.."),mlcd#INVERSE,mlcd#LARGE)
  Pause(64)
  mlcd.drawtext(32,@num1, mlcd#NORMAL, mlcd#LARGE)
  mlcd.drawtext(64,@num2, mlcd#NORMAL, mlcd#LARGE)
  mlcd.drawtext(96,@num5, mlcd#NORMAL, mlcd#SMALL)
  mlcd.drawtext(112,@num6, mlcd#NORMAL, mlcd#SMALL)
  Pause(768)
  mlcd.drawtext(32,@num3, mlcd#NORMAL, mlcd#LARGE)
  mlcd.drawtext(64,@num4, mlcd#NORMAL, mlcd#LARGE)
  Pause(768)

PUB Randoms | idx, xxx
  ' ----- random number patterns
  xxx := cnt
  mlcd.clear
  mlcd.drawtext(0,string("Random.."),mlcd#INVERSE,mlcd#LARGE)
  repeat 4
    repeat idx from 32 to 126 step 2  ' bars of thickness=2 pixels
      result := ||(?xxx // 128)
      mlcd.drawBar(idx, result, mlcd#NORMAL, 2)
  Pause(1000)

PUB Sinusoids | idx
' ----- sinusoid patterns
  mlcd.clear
  mlcd.drawtext(0,string("Sine...."),mlcd#INVERSE,mlcd#LARGE)

  repeat idx from 0 to 167                             ' show 96 lines
    result := FullSine((idx*125+4096) // 8192)               ' 8192 = 2^13, one full cycle for hub sine table
    result ~>= 10                                     ' reduce result to range of +/- 64 pixels
    mlcd.drawBar(idx+32, result+64, mlcd#NORMAL, 1)
    ' Bar is one pixel line tall, line number from 32 to 95
  Pause(512)

  repeat idx from 0 to 167                             ' show 96 lines
    result := FullSine(idx*300 // 8192)                        ' 8192 = 2^13, one full cycle for hub sine table
    result ~>= 11                                     ' reduce result to range of +/- 32 pixels
    mlcd.drawDot(idx+32, result+64, mlcd#INVERSE, 5)
    ' line number from 32 to 95, offset sine dot to center of display, dot is 5 pixel width
  Pause(768)

PUB WaveFonts
' ----- special font patterns, text pulled from DATa strings
  pause(500)
  mlcd.drawtext(0,string("Fonts..."),mlcd#INVERSE,mlcd#LARGE)
  mlcd.drawtext(32,@pat1,mlcd#NORMAL,mlcd#LARGE)
  mlcd.drawtext(64,@pat2,mlcd#NORMAL,mlcd#LARGE)
  mlcd.drawtext(96,@pat2,mlcd#NORMAL,mlcd#SMALL)
  mlcd.drawtext(112,@pat3,mlcd#NORMAL,mlcd#SMALL)
  pause(100)
  mlcd.drawtext(64,@pat2+3,mlcd#NORMAL,mlcd#LARGE)
  mlcd.drawtext(96,@pat2+3,mlcd#NORMAL,mlcd#SMALL)
  mlcd.drawtext(112,@pat3+10,mlcd#NORMAL,mlcd#SMALL)

  Pause(500)
  mlcd.Clear
  Pause(100)

PUB LargeFontList | idx
' ----- list all characters from large font, 4 screens of 32 chars
  repeat idx from 0 to 3
    BuildFont(idx*8)
    mlcd.drawtext(idx*32,@recordBuffer,mlcd#NORMAL,mlcd#LARGE)
  repeat idx from 0 to 3
    BuildFont(idx*8+64)
    mlcd.drawtext(idx*32,@recordBuffer,mlcd#NORMAL,mlcd#LARGE)
  repeat idx from 0 to 3
    BuildFont(idx*8+128)
    mlcd.drawtext(idx*32,@recordBuffer,mlcd#NORMAL,mlcd#LARGE)
  repeat idx from 0 to 3
    BuildFont(idx*8+192)
    mlcd.drawtext(idx*32,@recordBuffer,mlcd#NORMAL,mlcd#LARGE)
  Pause(768)
  mlcd.clear
  Pause(100)

PUB SmallFontList | idx
' ----- list all characters from small font, 2 screens of 64 chars
  repeat idx from 0 to 7
    BuildFont(idx*16)
    mlcd.drawtext(idx*16,@recordBuffer,mlcd#NORMAL,mlcd#SMALL)
  Pause(100)
  repeat idx from 0 to 7
    BuildFont(idx*16+128)
    mlcd.drawtext(idx*16,@recordBuffer,mlcd#NORMAL,mlcd#SMALL)
  Pause(768)

PUB SquaresTriangles | idx
' ----- squsre & triangle patterns
  mlcd.Clear
  repeat idx from 0 to 127
    mlcd.drawBar(idx,64, mlcd#NORMAL, 1)
  repeat idx from 0 to 127
    mlcd.drawBar(idx,64, mlcd#INVERSE, 1)
  mlcd.Clear
  repeat idx from 0 to 127
    mlcd.drawBar(idx,idx, mlcd#NORMAL, 1)
  repeat idx from 0 to 127
    mlcd.drawBar(idx,idx,  mlcd#INVERSE, 1)
  mlcd.Clear
  repeat idx from 0 to 127
    mlcd.drawBar(idx,127-idx, mlcd#NORMAL, 1)
  repeat idx from 0 to 127
    mlcd.drawBar(idx,127-idx,  mlcd#INVERSE, 1)

PUB ScreenSaver | idx, jdx
  ' ---- EME logo screen saver
  mlcd.clear
  mlcd.drawtext(0,string("That's all folks"),mlcd#NORMAL,mlcd#SMALL)
  mlcd.drawtext(16,string("Tnx for watching"),mlcd#NORMAL,mlcd#SMALL)
  DrawEME(mlcd#NORMAL)
  Pause(1500)

  jdx:=52
  idx:=52
  repeat
    mlcd.Clear
    Logo(||?idx//104, ||jdx?//96)
    Pause(1024)

PUB DrawEME(mode)
  mlcd.drawtext(32,string("EME     "),mode,mlcd#LARGE)
  mlcd.drawtext(64,string("SYSTEMS "),mode,mlcd#LARGE)
  mlcd.drawtext(96,string("    LLC "),mode,mlcd#LARGE)


PUB Logo(y,x) | idx, x1, x2, data[4]
  ' y,x = 0,0 is upper right corner.
  repeat idx from 0 to 23         ' there are 23 lines in the eme1 logo
    longfill(@data,$ffffffff,4)   ' this is a local 4-word line buffer, all $ff for white background
    result := !(long[@eme1][idx]><32)    ' pull the ith line of the logo out of memory
    ' reverse pixels because they are stored lsb first in memory, want then to come out msb first
    ' complement because a 1 pixiel is white, 0 is black
    x1 := (128-x)/32   ' which long of 0,1,2, or 3          ......./......./......./........
    x2 := (128-x)//32  ' logo may cross the boundare of two longs.  remainder from 0 to 31
    data[x1] := (-1 << x2) | (result >> (32-x2))  ' leftmost part of logo, right justified in its long
    if x1       ' it is at least 32 pix from the right edge
      if x2 == 0   ' right on a long boundary
        data[x1-1] := result
      else            ' split it
        data[x1-1] := (-1 >> (32-x2))| (result <<  x2)
    mlcd.DrawLine(@data,y+idx)

PUB BuildFont(start) | idx
  repeat idx from 0 to 15
    if start == 0
      start := 32    ' can't print the null char
    recordBuffer[idx] := start++
  recordBuffer[16] := 0


PUB FullSine(x) | y, q                                  ' x is 0 to 2^13 (0 to 8191) for 0 to 360 degrees
  q := x >> 11                                          ' quadrant
  y := (x & $7ff) << 1                                    ' 0 to 90- degrees
  case q                                                ' by quadrant
    0 : result := word[$E000 + y]                       '
    1 : result := word[$F001 - y]
    2 : result := -word[$E000 + y]
    3 : result := -word[$F001 - y]
  return                                                ' word +/- 32767 as plus/minus unity

PUB Pause(ms)
  waitcnt(clkfreq/1000*ms+cnt)

DAT
  pat1 byte 144,189,190,155,171,144,153,88
  pat2 byte 129,130,134,133,130,133,130,133,129,129,130,134,136,141,129,129,130,134,136,141,129
  pat3 byte "abcdefghijklmnopqrstuvwxyz1234567890"
  num1 byte 181,"g=110.1",0
  num2 byte "ppm=3",0
  num3 byte 176,"C= 23.7",0
  num4 byte "%RH=61.5",0
  num5 byte "37.863N 122.297W",0
  num6 byte " 25.10.16 13:22",0
  ' if a zstring is too short to fill the 8 or 16 char field, DrawText will substitute spaces

  long
  eme1  long %00010000000000000000000000001000
        long %00100000000000000000000000000100
        long %01000100000000000000000000100010
        long %01001000100000000000000100010010
        long %01001001000100000000100010010010
        long %01001001001110000001110010010010
        long %01001001001110000001110010010010
        long %01001001000100000000100010010010
        long %01001000100100000000100100010010
        long %01000100000100000000100000100010
        long %00100000000100000000100000000100
        long %00000000000010000001000000000000
        long %00000011111111000011111111000000
        long %00000100000001100110000000100000
        long %00001001111000111100111100010000
        long %00010010011100011001001110001000
        long %00010100011110011010001111001000
        long %00010100011110011010001111001000
        long %00010100000000011010000000001000
        long %00010010000110011001000011001000
        long %00010001001100011000100110001000
        long %00010000110000011000011000001000
        long %00010000000000011000000000001000
        long %00010000000000011000000000001000

CON
{{
;==============================================================================================================================;
|                                                   TERMS OF USE: MIT License                                                  |
|------------------------------------------------------------------------------------------------------------------------------|
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
;==============================================================================================================================;
}}
